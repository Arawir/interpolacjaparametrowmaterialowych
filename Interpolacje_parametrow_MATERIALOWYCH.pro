#-------------------------------------------------
#
# Project created by QtCreator 2020-03-31T00:00:36
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Interpolacje_parametrow_MATERIALOWYCH
TEMPLATE = app


SOURCES += main.cpp

HEADERS  += \
    core_database.h \
    core_semiconductor.h \
    core_engine.h \
    core_iengine.h \
    core_layer.h \
    mui_chart.h \
    mui_environment.h \
    mui_mainwindow.h \
    mui_mainlayout.h \
    mui_layer.h \
    mui_imainlayout.h \
    common_iconnector.h \
    mui_variable.h \
    mui_basicobject.h \
    common_raports.h \
    quantumwell.h \
    mui_transitions.h

FORMS    += mainwindow.ui
