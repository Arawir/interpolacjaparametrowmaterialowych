#ifndef CORE_DATABASE
#define CORE_DATABASE

#include <list>
#include <vector>
#include <string>
#include <cassert>
#include <iostream>
#include <fstream>

#include "core_semiconductor.h"



typedef unsigned int uint;

namespace Core{

    struct Element
    {
        std::string mName;
        uint mGroup;
        uint mPeriod;
    };


    class Database
    {
    public:
        double mTemperature;
        double mSubstrateGridConstant; //alphaE
        bool useSubstrate;
    private:
        std::list<Element> mElements;
        std::list<Semiconductor> mSemiconductors;

    public:
        Database() :
            mTemperature{ 0.0 }
          , mElements{ }
          , mSemiconductors{ }
        {

        }
        ~Database() = default;

        void loadDatabase()
        {
            std::fstream file("./database", std::ios::in);

            std::string tmp;

            while( !file.eof() ){
                file >> tmp;
                if(tmp == "new_semiconductor"){
                    mSemiconductors.push_back( Semiconductor{} );
                    continue;
                }
                if(tmp == "name"){
                    std::string tName;
                    file >> tName;
                    mSemiconductors.back().setName(tName);
                    continue;
                }
                if(tmp == "parameter"){
                    std::string tParameterName;
                    double tValue;
                    file >> tParameterName;
                    file >> tValue;
                    mSemiconductors.back().addParameter( {tParameterName, tValue} );
                    continue;
                }

                if(tmp == "new_element"){
                    mElements.push_back( Element{} );
                    continue;
                }
                if(tmp == "element_name"){
                    std::string tName;
                    file >> tName;
                    mElements.back().mName = tName;
                    continue;
                }
                if(tmp == "group"){
                    uint tGroup;
                    file >> tGroup;
                    mElements.back().mGroup = tGroup;
                    continue;
                }
                if(tmp == "period"){
                    uint tPeriod;
                    file >> tPeriod;
                    mElements.back().mPeriod = tPeriod;
                    continue;
                }

            }
            file.close();
        }

        Element* element(std::string nName)
        {
            for(auto& element : mElements){
                if(element.mName == nName){ return &element; }
            }
            std::cerr << "Cannot find chemical element in database! - " + nName;
            assert(false);
            return nullptr;
        }

        Semiconductor* semiconductor(std::string nName)
        {
            for(auto& semiconductor : mSemiconductors){
                if(semiconductor.mName == nName){ return &semiconductor; }
            }
            std::cerr << "Cannot find semiconductor in database! - " + nName;
            assert(false);
            return nullptr;
        }

        bool parameterHasBowing(std::string nParameter)
        {
            for(auto& semiconductor : mSemiconductors){
                if(semiconductor.parameterHasBowing(nParameter)){ return true; }
            }
            return false;
        }
    };

}
#endif // CORE_DATABASE

