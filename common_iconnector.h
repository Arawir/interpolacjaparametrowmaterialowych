#ifndef COMMON_ICONNECTOR
#define COMMON_ICONNECTOR

#include "common_raports.h"

//#define out std::cout
//#define nl std::endl

typedef unsigned int uint;

struct EnergyStructurePoint
{
    double mVariable;
    double mEc;
    double mEhh;
    double mElh;
    double mEsh;

    EnergyStructurePoint(double nVariable, double nEc, double nEhh, double nElh, double nEsh) :
        mVariable{ nVariable }
      , mEc{ nEc }
      , mEhh{ nEhh }
      , mElh{ nElh }
      , mEsh{ nEsh }
    {

    }
};


class iConnector
{
public:
    iConnector() = default;
    virtual ~iConnector() = default;

    virtual Raport addLayer(uint nId) = 0;
    virtual void removeLayer(uint nId) = 0;
    virtual Raport setLayerFormula(uint nLayerId, std::string nLayerFormula) = 0;
    virtual Raport setLayerWidth(uint nLayerId, double nWidth) = 0;
    virtual Raport setLayerAsSubstrate(uint nLayerId) = 0;
    virtual void unsetSubstrate() = 0;
    virtual double layerEc(uint nLayerId) = 0;
    virtual double layerEhh(uint nLayerId) = 0;
    virtual double layerElh(uint nLayerId) = 0;
    virtual double layerEsh(uint nLayerId) = 0;
    virtual double layerEg(uint nLayerId) = 0;
    virtual double layerMe(uint nLayerId) = 0;
    virtual double layerMlh(uint nLayerId) = 0;
    virtual double layerMhh(uint nLayerId) = 0;
    virtual std::vector<EnergyStructurePoint> layerEnergyStructure(uint nLayerId, uint nVariableId) = 0;
    virtual bool layerEnergyStructureIsEneable(uint nLayerId, uint nVariableId) = 0;


    virtual Raport addVariable(uint nId) = 0;
    //virtual void removeVariable(uint nId) = 0;
    virtual Raport setVariableValue(uint nId, double nValue) = 0;
    virtual Raport setVariableName(uint nId, std::string nName) = 0;

    virtual Raport setTemperature(double nTemperature) = 0;
};

#endif // COMMON_ICONNECTOR

