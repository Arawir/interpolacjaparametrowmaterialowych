#ifndef QUANTUMWELL
#define QUANTUMWELL

#include <vector>
#include <cassert>

typedef unsigned int uint;

double abs(double value)
{
    if(value<0.0) return -value;
    return value;
}

struct Eigen
{
    double mValue;
    std::vector<double> mVector;
};


class QuantumWell
{
protected:
    std::vector<double> z;
    std::vector<double> vCb;
    std::vector<double> vLh;
    std::vector<double> vHh;
    std::vector<double> vSh;

    std::vector<double> mCb;
    std::vector<double> mLh;
    std::vector<double> mHh;
    std::vector<double> mSh;

    Eigen eigCb,eigLh,eigHh,eigSh;
    uint gridPoints;
    double dz;

    double q = 1.602e-19;
    double m0 = 9.109e-31;
    double hb = 1.05459e-34;
    double h = 6.626e-34;
    double c = 2.998E+8;
    double Vc = 1.0;
    double Lc=1.0;
    double precision = 1E-6;
    double beta = 0.0;

public:
    QuantumWell(){ }

    void setGridPoints(uint nGridPoints)
    {
        gridPoints = nGridPoints;
        z.resize(gridPoints);

        vCb.resize(gridPoints);
        vLh.resize(gridPoints);
        vHh.resize(gridPoints);
        vSh.resize(gridPoints);

        mCb.resize(gridPoints);
        mLh.resize(gridPoints);
        mHh.resize(gridPoints);
        mSh.resize(gridPoints);

        beta = 2.0*m0*Vc*pow(Lc*dz/hb,2);
    }

    void recalculate()
    {
        recalculateCb();
        recalculateLh();
        recalculateHh();
        recalculateSh();
    }

    double transEnergyEv(std::string band)
    {
        if(band=="lh"){ return (eigCb.mValue-eigLh.mValue)/q; }
        if(band=="hh"){ return (eigCb.mValue-eigHh.mValue)/q; }
        if(band=="sh"){ return (eigCb.mValue-eigSh.mValue)/q; }
        assert(false); return 0;
    }

    double transWavelengthUm(std::string band)
    {
        if(band=="lh"){ return h*c/(eigCb.mValue-eigLh.mValue)*1.0E6; }
        if(band=="hh"){ return h*c/(eigCb.mValue-eigHh.mValue)*1.0E6; }
        if(band=="sh"){ return h*c/(eigCb.mValue-eigSh.mValue)*1.0E6; }
        assert(false); return 0;
    }

    double transOverlap(std::string band)
    {
        double oOverlap =0.0;

        if(band=="lh"){
            for(uint i=0; i<z.size(); i++){
                oOverlap += eigCb.mVector[i]*eigLh.mVector[i];
            }
        }
        if(band=="hh"){
            for(uint i=0; i<z.size(); i++){
                oOverlap += eigCb.mVector[i]*eigHh.mVector[i];
            }
        }
        if(band=="sh"){
            for(uint i=0; i<z.size(); i++){
                oOverlap += eigCb.mVector[i]*eigSh.mVector[i];
            }
        }
        return oOverlap;
    }

private:
    void recalculateCb()
    {
        if(minimumAtEdge(vCb)){
            eigCb.mValue = vCb.front();
            eigCb.mVector = std::vector<double>(vCb.size(), 0.0);
        } else {
            std::vector<double> e(gridPoints);
            std::vector<double> d(gridPoints);

            for(int i=0; i<(int)gridPoints; i++){
                d[i] = wCb(i-1,i)+wCb(i,i+1) + vCb[i]*beta;
                e[i] = -wCb(i,i+1);
            }
            uint pivotPos = positionWithMinimum(vCb);
            eigCb.mValue = caclulateEigenvalue(e,d,0,precision)/beta;
            eigCb.mVector = calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), pivotPos);
            if(mabs(vCb[gridPoints-pivotPos]-vCb[pivotPos])<1.0E-12){
                eigCb.mVector = addVectors(eigCb.mVector, calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), gridPoints-pivotPos));
            }
            repositiveVector(eigCb.mVector);
        }
    }

    void recalculateLh()
    {
        //if(maximumAtEdge(vLh)){
        if(false){
            eigLh.mValue = vLh.front();
            eigLh.mVector = std::vector<double>(vLh.size(), 0.0);
        } else {
            std::vector<double> e(gridPoints);
            std::vector<double> d(gridPoints);

            for(uint i=0; i<gridPoints; i++){
                d[i] = wLh(i-1,i)+wLh(i,i+1) - vLh[i]*beta;
                e[i] = -wLh(i,i+1);
            }
            uint pivotPos = positionWithMaximum(vLh);
            eigLh.mValue = -caclulateEigenvalue(e,d,0,precision)/beta;
            eigLh.mVector = calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), pivotPos);
            if(mabs(vLh[gridPoints-pivotPos]-vLh[pivotPos])<1.0E-12){
                eigLh.mVector = addVectors(eigLh.mVector, calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), gridPoints-pivotPos));
            }
            repositiveVector(eigLh.mVector);
        }
    }

    void recalculateHh()
    {
       // if(maximumAtEdge(vHh)){
        if(false){
            eigHh.mValue = vHh.front();
            eigHh.mVector = std::vector<double>(vHh.size(), 0.0);
        } else {
            std::vector<double> e(gridPoints);
            std::vector<double> d(gridPoints);

            for(uint i=0; i<gridPoints; i++){
                d[i] = wHh(i-1,i)+wHh(i,i+1) - vHh[i]*beta;
                e[i] = -wHh(i,i+1);
            }
            uint pivotPos = positionWithMaximum(vHh);
            eigHh.mValue = -caclulateEigenvalue(e,d,0,precision)/beta;
            eigHh.mVector = calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), pivotPos);
            if(mabs(vHh[gridPoints-pivotPos]-vHh[pivotPos])<1.0E-12){
                eigHh.mVector = addVectors(eigHh.mVector, calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), gridPoints-pivotPos));
            }
            repositiveVector(eigHh.mVector);
        }
    }
    void recalculateSh()
    {
      //  if(maximumAtEdge(vSh)){
        if(false){
            eigSh.mValue = vSh.front();
            eigSh.mVector = std::vector<double>(vSh.size(), 0.0);
        } else {
            std::vector<double> e(gridPoints);
            std::vector<double> d(gridPoints);

            for(uint i=0; i<gridPoints; i++){
                d[i] = wSh(i-1,i)+wSh(i,i+1) - vSh[i]*beta;
                e[i] = -wSh(i,i+1);
            }
            uint pivotPos = positionWithMaximum(vSh);
            eigSh.mValue = -caclulateEigenvalue(e,d,0,precision)/beta;
            eigSh.mVector = calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), pivotPos);
            if(mabs(vSh[gridPoints-pivotPos]-vSh[pivotPos])<1.0E-12){
                eigSh.mVector = addVectors(eigSh.mVector, calculateEigenvector(e,d,caclulateEigenvalue(e,d,0,precision), gridPoints-pivotPos));
            }
            repositiveVector(eigSh.mVector);
        }
    }

    void normalise(std::vector<double> &V)
    {
        double norm =0.0;

        for(auto &v : V){ norm += v*v; }
        norm = sqrt(norm);
        for(auto &v : V){ v /= norm; }
    }

    uint ujemneU(double z, const std::vector<double> &e, const std::vector<double> &d)
    {
        uint oLiczbaUjemnychU=0;

        double u = d[0]-z;
        if(u< 0.0){ oLiczbaUjemnychU++; }

        for(uint i=1; i<d.size();i++){
            u = d[i]-z-pow(e[i-1],2)/u;
            if(u< 0.0){ oLiczbaUjemnychU++; }
        }
        return oLiczbaUjemnychU;
    }

    double caclulateEigenvalue(std::vector<double> e, std::vector<double> d, uint M, double precission)
    {
        double minZ =-1.0;
        while(ujemneU(minZ,e,d)>M){
            minZ *= 2.0;
        }

        double maxZ = 1.0;
        while(ujemneU(maxZ,e,d)<=M){
            maxZ *= 2.0;
        }

        while((maxZ-minZ)>precission){
            double midZ = (maxZ+minZ)/2.0;
            if( ujemneU(midZ,e,d)<=M ){ minZ = midZ; }
            else{ maxZ = midZ; }
        }

        return (maxZ+minZ)/2.0;
    }

    std::vector<double> calculateEigenvector(std::vector<double> e, std::vector<double> d2, double nEigenvalue, int i0)
    {
        int nz = d2.size();
        std::vector<double> oVec(nz, 0.0);
        std::vector<double> m(nz);
        std::vector<double> p(nz);


        m[0] = 1.0/(d2[0]-nEigenvalue);
        for(int j=1;j<nz;j++){
            m[j]=1.0/(d2[j]-nEigenvalue-pow(e[j],2)*m[j-1]);
        }

        p[nz-1] = 1.0/(d2[nz-1]-nEigenvalue);
        for(int i=nz-2;i>=0;i--){
            p[i]=1.0/(d2[i]-nEigenvalue-pow(e[i+1],2)*p[i+1]);
        }

        oVec[i0]=10.0;

        for(int j=i0+1;j<nz;j++){
            oVec[j]=-e[j]*p[j]*oVec[j-1];
        }
        for(int j=i0-1;j>=0; j--){
            oVec[j]=-e[j+1]*m[j]*oVec[j+1];
        }

        normalise(oVec);
        return oVec;
    }

    double mabs(double val)
    {
        if(val<0.0){ return -val; }
        return val;
    }

    int positionWithMinimum(std::vector<double> &vec)
    {
        uint oPosA = 0;
        uint oPosB = 0;
        double lMin = vec[0]+1.0;

        for(uint i=0; i<vec.size(); i++){
            if(vec[i]<lMin){
                oPosA=i; lMin = vec[i];
                while((mabs(vec[i+1]-lMin)<1.0E-22)&(i+1<vec.size())){
                    i++;
                }
                oPosB=i;
            }
        }

        return (int)(oPosB+oPosA)/2;
    }

    int positionWithMaximum(std::vector<double> &vec)
    {
        uint oPosA = 0;
        uint oPosB = 0;
        double lMax = vec[0]-1.0;

        for(uint i=0; i<vec.size(); i++){
            if(vec[i]>lMax){
                oPosA=i; lMax = vec[i];
                while((mabs(vec[i+1]-lMax)<1.0E-22)&(i+1<vec.size())){
                    i++;
                }
                oPosB=i;
            }
        }

        return (int)(oPosB+oPosA)/2;
    }


    bool minimumAtEdge(std::vector<double> &vec)
    {
        auto iter = std::min_element(vec.begin(), vec.end());
        double lMin = *iter;
        if(mabs(lMin-vec.front()) < mabs(vec.front())/1000.0){ return true; }
        if(mabs(lMin-vec.back()) < mabs(vec.back())/1000.0){ return true; }
        return false;
    }

    bool maximumAtEdge(std::vector<double> &vec)
    {
        auto iter = std::max_element(vec.begin(), vec.end());
        double lMax = *iter;
        if(mabs(lMax-vec.front()) > mabs(vec.front())/1000.0){ return true; }
        if(mabs(lMax-vec.back()) > mabs(vec.back())/1000.0){ return true; }
        return false;
    }

    void repositiveVector(std::vector<double> &vec)
    {
        for(auto &val : vec){
            if(val<0.0) val = -val;
        }
    }

    std::vector<double> addVectors(std::vector<double>& a, std::vector<double> b)
    {
        std::vector<double> out(a.size(), 0.0);

        for(uint i=0; i<a.size(); i++){
            out[i] = (a[i]+b[i])/2.0;
        }
        return out;
    }

    double wCb(int i, int j)
    {
        if(i==-1){ i = 0; }
        if(j==(int)mCb.size()){ j=(int)mCb.size()-1; }
        return 2.0/(mCb[i]+mCb[j]);
    }

    double wLh(int i, int j)
    {
        if(i==-1) i = 0;
        if(j==(int)mLh.size()){ j=(int)mLh.size()-1; }
        return 2.0/(mLh[i]+mLh[j]);
    }

    double wHh(int i, int j)
    {
        if(i==-1) i = 0;
        if(j==(int)mHh.size()){ j=(int)mHh.size()-1; }
        return 2.0/(mHh[i]+mHh[j]);
    }

    double wSh(int i, int j)
    {
        if(i==-1) i = 0;
        if(j==(int)mSh.size()){ j=(int)mSh.size()-1; }
        return 2.0/(mSh[i]+mSh[j]);
    }
};

#endif // QUANTUMWELL

