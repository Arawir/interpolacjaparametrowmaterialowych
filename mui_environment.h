#ifndef MUI_ENVIRONMENT
#define MUI_ENVIRONMENT

#include <QHBoxLayout>
#include <QPushButton>
#include <QFrame>
#include <QLineEdit>

#include "mui_basicobject.h"

namespace Mui{

    class EnvironmentBox : public BasicObject
    {
        Q_OBJECT
    private:
        QHBoxLayout* mLayout;
        QLineEdit* mTemperatureEdit;
        QLineEdit* mFileNameEdit;
        QLineEdit* mDiscPointsEdit;

    public:
        EnvironmentBox(iMainLayout& nMainLayout) :
            BasicObject{ nMainLayout }
        {
            setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

            mLayout = new QHBoxLayout{};
            mLayout->setAlignment(Qt::AlignTop);
            setLayout(mLayout);

            mTemperatureEdit = new QLineEdit{ };
            mTemperatureEdit->setPlaceholderText("Temperature [K]");
            mTemperatureEdit->setMaximumHeight(30);
            mLayout->addWidget(mTemperatureEdit);

            mFileNameEdit = new QLineEdit{ };
            mFileNameEdit->setPlaceholderText("File name");
            mFileNameEdit->setMaximumHeight(30);
            mLayout->addWidget(mFileNameEdit);

            mDiscPointsEdit = new QLineEdit{ };
            mDiscPointsEdit->setPlaceholderText("Number of points to save");
            mDiscPointsEdit->setMaximumHeight(30);
            mLayout->addWidget(mDiscPointsEdit);

            mDiscPointsEdit->setText("1001");
            mTemperatureEdit->setText("300");
        }
        ~EnvironmentBox() = default;

        QString fileName()
        {
            return mFileNameEdit->text();
        }

        uint numOfPoints()
        {
            return mDiscPointsEdit->text().toUInt();
        }

        void update() override
        {
            mMainLayout.setTemperature(mTemperatureEdit->text().toDouble());
        }

    };

}

#endif // MUI_ENVIRONMENT

