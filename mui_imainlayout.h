#ifndef MUI_IMAINLAYOUT
#define MUI_IMAINLAYOUT

#include <QtCharts/QAreaSeries>
#include "common_iconnector.h"



namespace Mui{

    class VariableBox;
    class LayerBox;

    class iMainLayout : public iConnector
    {
    public:
        iMainLayout(){ }
        virtual ~iMainLayout() = default;

        virtual QtCharts::QLineSeries* conQuantumWellPoints() = 0;
        virtual QtCharts::QLineSeries* hhQuantumWellPoints() = 0;
        virtual QtCharts::QLineSeries* lhQuantumWellPoints() = 0;
        virtual QtCharts::QLineSeries* shQuantumWellPoints() = 0;
        virtual uint layerNumber(uint nLayerId) = 0;
        virtual void saveData() = 0;
        virtual QString layerFormula(uint nLayerId) = 0;

        virtual std::vector<VariableBox*> &variables() = 0;
        virtual std::vector<LayerBox*> &layers() = 0;

        virtual void resetSubstrateInsteadOf(uint nLayerId) = 0;
        virtual void updateWithoutChartBox() = 0;
        virtual double transParam(std::string band, std::string param) = 0;
        virtual void calculateEigens() = 0;

        virtual QtCharts::QAreaSeries* eigenfunction(QString band, double amplitude) = 0;

    };

}

#endif // MUI_IMAINLAYOUT

