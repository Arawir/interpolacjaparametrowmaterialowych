#ifndef CORE_IENGINE
#define CORE_IENGINE

#include "common_iconnector.h"

namespace Core{

    struct QuantumWellPoint
    {
        double mX;
        double mEnergy;
    };



    struct Variable
    {
        double mValue;
        std::string mName;
        uint mId;
    };

    class iEngine : public iConnector
    {
    public:
        iEngine(){ }
        virtual ~iEngine() = default;

        virtual Variable* variable(std::string nName) = 0;
        virtual bool existsParameter(std::string nName) = 0;

        virtual void saveData(std::string filename, uint numberOfPoints) = 0;

        virtual std::vector<QuantumWellPoint> hhBandPoints() = 0;
        virtual std::vector<QuantumWellPoint> lhBandPoints() = 0;
        virtual std::vector<QuantumWellPoint> shBandPoints() = 0;
        virtual std::vector<QuantumWellPoint> conBandPoints() = 0;
        virtual double locationOfBeginningOfLayer(uint nLayerId) = 0;
        virtual double locationOfEndingOfLayer(uint nLayerId) = 0;

        virtual double transParam(std::string band, std::string param) = 0;
        virtual std::vector<QuantumWellPoint> eigenfunction(std::string band, double amplitude) = 0;
        virtual std::vector<QuantumWellPoint> eigenEnergyLine(std::string band) = 0;
        virtual void update(uint numOfPoionts) = 0;
    };

}

#endif // CORE_IENGINE

