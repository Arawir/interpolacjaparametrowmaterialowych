#ifndef CORE_SEMICONDUCTOR
#define CORE_SEMICONDUCTOR

#include <string>

namespace Core{

    struct Parameter
    {
        std::string mName;
        double mValue;

        Parameter(std::string nName, double nValue) :
            mName{ nName }
          , mValue{ nValue }
        {

        }
    };

    class Semiconductor
    {
    public:
        std::string mName;
        std::vector<Parameter> mParameters;

        void setName(std::string nName)
        {
            mName = nName;
        }

        void addParameter(Parameter nParameter)
        {
            mParameters.push_back(nParameter);
        }

        double valueOf(std::string nParameter)
        {
            for(auto& parameter : mParameters){
                if(parameter.mName == nParameter){ return parameter.mValue; }
            }
            assert(!"Cannot find parameter!");
            return 0.0;
        }

        bool parameterHasBowing(std::string nParameter)
        {
            //assert( parameterExists(nParameter) );
            for(auto& parameter : mParameters){
                if(parameter.mName == (nParameter+"Bowing")){ return true; }
            }
            return false;
        }

    private:
        bool parameterExists(std::string nParameter)
        {
            for(auto& parameter : mParameters){
                if(parameter.mName == nParameter){ return true; }
            }
            return false;
        }
    };
}

#endif // CORE_SEMICONDUCTOR

