#ifndef MUI_LAYER
#define MUI_LAYER

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QValidator>


#include "mui_basicobject.h"

namespace Mui{

    class LayerBox : public BasicObject
    {
        Q_OBJECT
    private:
        QVBoxLayout* mLayout1;
        QHBoxLayout* mLayout2;
        QHBoxLayout* mLayout3;

        NamedLineEdit* mWidth;
        NamedLineEdit* mFormula;

        QPushButton* mRemove;
        QPushButton* mProperties;
        QCheckBox* mSubstrateButton;

        QLabel* mPropertiesLabel;

    public:
        LayerBox(iMainLayout& nMainLayout) :
           BasicObject{ nMainLayout }
        {
            setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
            setMaximumWidth(400);

            mLayout1 = new QVBoxLayout{};
            setLayout(mLayout1);

            mLayout2 = new QHBoxLayout{};
            mLayout1->addLayout(mLayout2);

            mLayout3 = new QHBoxLayout{};
            mLayout1->addLayout(mLayout3);

            mFormula = new NamedLineEdit{"Formula", "" };
            mLayout2->addWidget(mFormula);

          //  mWidth = new NamedLineEdit{"Width [\u212B]", ""};
            mWidth = new NamedLineEdit{"Width [nm]", ""};
            mLayout2->addWidget(mWidth);

            mSubstrateButton = new QCheckBox{"Substrate"};
            connect(mSubstrateButton, SIGNAL(stateChanged(int)), this, SLOT(setSubstrate(int)));
            mLayout2->addWidget(mSubstrateButton);

            mRemove = new QPushButton{ "Remove"};
            connect(mRemove, SIGNAL(clicked(bool)), this, SLOT(remove()));
            mLayout3->addWidget(mRemove);

            mProperties = new QPushButton{ "Properties"};
            connect(mProperties, SIGNAL(clicked(bool)), this, SLOT(showHideProperties()));
            mLayout3->addWidget(mProperties);

            mPropertiesLabel = new QLabel{ "Layer properties" };
            mLayout1->addWidget(mPropertiesLabel);
            mPropertiesLabel->hide();

            setFrameStyle(QFrame::StyledPanel | QFrame::Plain);

        }
        ~LayerBox() = default;


        QString name() override
        {
            return "Layer nr. " + QString::number(mMainLayout.layerNumber( id() ));
        }

        void update() override
        {
            mMainLayout.setLayerFormula(mId, mFormula->text().toStdString());
            mMainLayout.setLayerWidth(mId, mWidth->text().toDouble()*10.0); //do Angstremow

            if(mSubstrateButton->isChecked()){ mMainLayout.setLayerAsSubstrate(mId); }
            updateProperties();
        }

        void resetSubstrateButton()
        {
            mSubstrateButton->setCheckState(Qt::CheckState::Unchecked);
        }

        QString formula()
        {
            return mFormula->text();
        }

        void setParameterts(QString formula, QString width, bool substrate)
        {
            mFormula->setText(formula);
            mWidth->setText(width);
            mSubstrateButton->setChecked(substrate);
        }

    private:
        void updateProperties()
        {
            QString lProperties = "";

            lProperties += "Energy gap : ";
            lProperties += QString::number( mMainLayout.layerEg(mId));
            lProperties += " [eV] \r\n";

            lProperties += "Ec : ";
            lProperties += QString::number( mMainLayout.layerEc(mId));
            lProperties += " [eV] \r\n";

            lProperties += "Ehh : ";
            lProperties += QString::number( mMainLayout.layerEhh(mId));
            lProperties += " [eV] \r\n";

            lProperties += "Elh : ";
            lProperties += QString::number( mMainLayout.layerElh(mId));
            lProperties += " [eV] \r\n";

            lProperties += "Esh : ";
            lProperties += QString::number( mMainLayout.layerEsh(mId));
            lProperties += " [eV] \r\n";

            lProperties += "me/m0 : ";
            lProperties += QString::number( mMainLayout.layerMe(mId));
            lProperties += " \r\n";

            lProperties += "mlh/m0 : ";
            lProperties += QString::number( mMainLayout.layerMlh(mId));
            lProperties += " \r\n";

            lProperties += "mhh/m0 : ";
            lProperties += QString::number( mMainLayout.layerMhh(mId));
            lProperties += " \r\n";


            mPropertiesLabel->setText( lProperties );
        }


    private slots:
        void showHideProperties()
        {
            if(mPropertiesLabel->isHidden()){
                mPropertiesLabel->show();
            } else {
                mPropertiesLabel->hide();
            }
        }

        void setSubstrate(int nButtonState)
        {
            if(nButtonState==Qt::CheckState::Checked){
                mMainLayout.resetSubstrateInsteadOf( mId );
            }
        }

        void remove()
        {
            mMainLayout.removeLayer(mId);
        }

    };

}

#endif // MUI_LAYER

