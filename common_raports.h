#ifndef COMMON_RAPORTS
#define COMMON_RAPORTS

#include <string>

enum class RaportType
{
    Ok, Bug
};

struct Raport
{
    RaportType mType;
    std::string mText;

    Raport( RaportType nType, std::string nText="") :
        mType{ nType }
      , mText{ nText }
    {

    }
};

#endif // COMMON_RAPORTS

