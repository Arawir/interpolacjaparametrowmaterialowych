#include "mui_mainwindow.h"
#include <QApplication>
#include <QDebug>

#include "core_layer.h"
#include "core_engine.h"


uint Mui::BasicObject::sLastObjectId = 0;

int main(int argc, char *argv[])
{

    qputenv("QT_STYLE_OVERRIDE","");

    Core::Database fDatabase;
    fDatabase.loadDatabase();

    Core::Engine fEngine{ fDatabase };

    QApplication a(argc, argv);

    MainWindow w{ fEngine };
    w.show();

    return a.exec();
}
