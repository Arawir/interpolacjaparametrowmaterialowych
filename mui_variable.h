#ifndef MUI_PARAMETER
#define MUI_PARAMETER

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>

#include "mui_imainlayout.h"
#include "mui_basicobject.h"

namespace Mui{


    class VariableBox : public BasicObject
    {
        Q_OBJECT
    private:
        QHBoxLayout* mLayout1;

        QLineEdit* mName;
        QLineEdit* mValue;

        QPushButton* mRemoveButton;

    public:
        VariableBox(iMainLayout& nMainLayout) :
           BasicObject{ nMainLayout }
        {
            setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
            setMaximumWidth(400);

            mLayout1 = new QHBoxLayout{};
            setLayout(mLayout1);

            mName = new QLineEdit{ };
            mName->setPlaceholderText("name");
            mName->setMaximumHeight(30);
            mLayout1->addWidget(mName);

            mValue = new QLineEdit{ };
            mValue->setPlaceholderText("value");
            mValue->setMaximumHeight(30);
            mLayout1->addWidget(mValue);

            mRemoveButton = new QPushButton{"Remove"};
            mLayout1->addWidget(mRemoveButton);

            setFrameStyle(QFrame::StyledPanel | QFrame::Plain);

        }
        ~VariableBox() = default;


        QString name() override
        {
            return mName->text();
        }

        void update() override
        {
            mMainLayout.setVariableName(mId, mName->text().toStdString());
            mMainLayout.setVariableValue(mId, mValue->text().toDouble());
        }

        void setParameters(QString name, QString value)
        {
            mName->setText(name);
            mValue->setText(value);
        }
    };

}

#endif // MUI_PARAMETER

