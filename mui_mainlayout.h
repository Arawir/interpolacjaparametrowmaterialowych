#ifndef MUI_MAINLAYOUT
#define MUI_MAINLAYOUT

#include <QVBoxLayout>
#include <QHBoxLayout>

#include "mui_imainlayout.h"

#include "core_iengine.h"
#include "mui_chart.h"
#include "mui_environment.h"

#include "mui_layer.h"
#include "mui_variable.h"
#include "mui_transitions.h"

namespace Mui{

    class MainLayout : public QWidget, public iMainLayout
    {
        Q_OBJECT
    public:
        Core::iEngine& mEngine;

        QHBoxLayout* mLayout1;
        QVBoxLayout* mLayout2;
        QHBoxLayout* mLayout2a;
        QVBoxLayout* mLayout2b;
        QVBoxLayout* mLayout2ba;
        QVBoxLayout* mLayout2bb;
        QVBoxLayout* mLayout3;

        QPushButton* mAddLayoutBox;
        QPushButton* mAddParameterBox;
        QPushButton* mUpdateButton;


        QScrollArea* mScrollArea;

        std::vector<LayerBox*> mLayerBoxes;
        std::vector<VariableBox*> mParameterBoxes;
        ChartBox* mChartBox;
        EnvironmentBox* mEnvironmentBox;
        TransitionsBox *mTransitionsBox;

    public:
        MainLayout( Core::iEngine& nEngine ) :
            QWidget{}
          , iMainLayout{ }
          , mEngine{ nEngine }
        {
            mLayout1 = new QHBoxLayout{ };
            setLayout(mLayout1);

            mLayout2 = new QVBoxLayout{ };
            mLayout2->setAlignment(Qt::AlignTop);
            mLayout1->addLayout(mLayout2);

            mLayout2a = new QHBoxLayout{ };
            mLayout2a->setAlignment(Qt::AlignTop);
            mLayout2->addLayout(mLayout2a);

            mLayout2b = new QVBoxLayout{ };
            mLayout2b->setAlignment(Qt::AlignTop);
            mScrollArea = new QScrollArea{};
            mScrollArea->setMaximumWidth(450);
            mScrollArea->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
            mScrollArea->setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
            //mScrollArea->setVerticalScrollBarPolicy( Qt::ScrollBarAsNeeded );
            mScrollArea->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOn );
            mScrollArea->setWidgetResizable( true );
            mScrollArea->setLayout(mLayout2b);
            mLayout2->addWidget(mScrollArea);

            mLayout2ba = new QVBoxLayout{ };
            mLayout2ba->setAlignment(Qt::AlignTop);
            mLayout2b->addLayout(mLayout2ba);

            mLayout2bb = new QVBoxLayout{ };
            mLayout2bb->setAlignment(Qt::AlignTop);
            mLayout2b->addLayout(mLayout2bb);

            mLayout3 = new QVBoxLayout{ };
            mLayout3->setAlignment(Qt::AlignTop);
            mLayout1->addLayout(mLayout3);


            mAddLayoutBox = new QPushButton{ "Add Layer" };
            mAddLayoutBox->setMaximumWidth(150);
            connect(mAddLayoutBox, SIGNAL(clicked(bool)), this, SLOT(addLayerBox()));
            mLayout2a->addWidget(mAddLayoutBox);

            mAddParameterBox = new QPushButton{ "Add Variable" };
            mAddParameterBox->setMaximumWidth(150);
            connect(mAddParameterBox, SIGNAL(clicked(bool)), this, SLOT(addParameterBox()));
            mLayout2a->addWidget(mAddParameterBox);

            mUpdateButton = new QPushButton{ "Update" };
            mUpdateButton->setMaximumWidth(150);
            connect(mUpdateButton, SIGNAL(clicked(bool)), this, SLOT(update()));
            mLayout2a->addWidget(mUpdateButton);


            mEnvironmentBox = new EnvironmentBox{*this};
            mLayout2->addWidget(mEnvironmentBox);

            mTransitionsBox = new TransitionsBox{*this};
            mLayout2->addWidget(mTransitionsBox);

            mChartBox = new ChartBox{*this};
            mLayout3->addWidget(mChartBox);


            setParameters();

        }
        ~MainLayout() = default;

        void setParameters()
        {
            addParameterBox("x","0.5");
            addParameterBox("y","1.0");
            addParameterBox("p","0.0");
            addParameterBox("q","1.0");

            addLayerBox("Ga-As","20",true);
            addLayerBox("Ga(x)-In(1-x)-As(y)-Sb(1-y)","2",false);
            addLayerBox("Al(p)-Ga(1-p)-As(q)-Sb(1-q)","2",false);
            addLayerBox("Ga(x)-In(1-x)-As(y)-Sb(1-y)","2",false);
            addLayerBox("Ga-As","20",false);

            update();
        }

        /////////////////////////////
        std::vector<VariableBox*> &variables() override
        {
            return mParameterBoxes;
        }

        std::vector<LayerBox*> &layers() override
        {
            return mLayerBoxes;
        }

        QtCharts::QLineSeries* conQuantumWellPoints() override
        {
            QtCharts::QLineSeries* oSeries = new QtCharts::QLineSeries{};
            for(auto& point : mEngine.conBandPoints()){
                oSeries->append(point.mX/10.0, point.mEnergy);
            }
            return oSeries;
        }

        QtCharts::QLineSeries* hhQuantumWellPoints() override
        {
            QtCharts::QLineSeries* oSeries = new QtCharts::QLineSeries{};
            for(auto& point : mEngine.hhBandPoints()){
                oSeries->append(point.mX/10.0, point.mEnergy);
            }
            return oSeries;
        }

        QtCharts::QLineSeries* lhQuantumWellPoints() override
        {
            QtCharts::QLineSeries* oSeries = new QtCharts::QLineSeries{};
            for(auto& point : mEngine.lhBandPoints()){
                oSeries->append(point.mX/10.0, point.mEnergy);
            }
            return oSeries;
        }

        QtCharts::QLineSeries* shQuantumWellPoints() override
        {
            QtCharts::QLineSeries* oSeries = new QtCharts::QLineSeries{};
            for(auto& point : mEngine.shBandPoints()){
                oSeries->append(point.mX/10.0, point.mEnergy);
            }
            return oSeries;
        }


        uint layerNumber(uint nLayerId) override
        {
            for(uint i=0; i<mLayerBoxes.size(); i++){
                if(mLayerBoxes[i]->id() == nLayerId){ return i; }
            }
            assert(false);
            return 0;
        }

        QString layerFormula(uint nLayerId) override
        {
            for(auto &layer : mLayerBoxes){
                if(layer->id() == nLayerId){
                    return layer->formula();
                }
            }
            assert(false);
            return "";
        }

        void resetSubstrateInsteadOf(uint nLayerId) override
        {
            for(auto& layerBox : mLayerBoxes){
                if(layerBox->id()!=nLayerId){
                    layerBox->resetSubstrateButton();
                }
            }
        }
        ///////////////////////

        Raport addLayer(uint nId) override
        {
            return mEngine.addLayer(nId);
        }

        void removeLayer(uint nId) override
        {
            mEngine.removeLayer(nId);

            LayerBox* tLayer;

            for(uint i=0; i<mLayerBoxes.size(); i++){
                if(mLayerBoxes.at(i)->id() == nId){
                    tLayer = mLayerBoxes.at(i);
                    mLayerBoxes.erase(mLayerBoxes.begin()+i);
                }
            }

            delete tLayer;
            update();
        }

        Raport setLayerFormula(uint nLayerId, std::string nLayerFormula) override
        {
            return mEngine.setLayerFormula(nLayerId, nLayerFormula);
        }

        Raport setLayerWidth(uint nLayerId, double nWidth) override
        {
            return mEngine.setLayerWidth(nLayerId, nWidth);
        }

        Raport setLayerAsSubstrate(uint nLayerId) override
        {
           return mEngine.setLayerAsSubstrate(nLayerId);
        }

        void unsetSubstrate() override
        {
            mEngine.unsetSubstrate();
        }

        double layerEc(uint nLayerId) override
        {
            return mEngine.layerEc(nLayerId);
        }

        double layerEhh(uint nLayerId) override
        {
            return mEngine.layerEhh(nLayerId);
        }

        double layerElh(uint nLayerId) override
        {
            return mEngine.layerElh(nLayerId);
        }

        double layerEsh(uint nLayerId) override
        {
            return mEngine.layerEsh(nLayerId);
        }

        double layerEg(uint nLayerId) override
        {
            return mEngine.layerEg(nLayerId);
        }

        double layerMe(uint nLayerId) override
        {
            return mEngine.layerMe(nLayerId);
        }
        double layerMlh(uint nLayerId) override
        {
            return mEngine.layerMlh(nLayerId);
        }
        double layerMhh(uint nLayerId) override
        {
            return mEngine.layerMhh(nLayerId);
        }

        std::vector<EnergyStructurePoint> layerEnergyStructure(uint nLayerId, uint nVariableId) override
        {
            return mEngine.layerEnergyStructure(nLayerId, nVariableId);
        }

        bool layerEnergyStructureIsEneable(uint nLayerId, uint nVariableId) override
        {
            return mEngine.layerEnergyStructureIsEneable(nLayerId, nVariableId);
        }

        Raport addVariable(uint nId) override
        {
            return mEngine.addVariable(nId);
        }

        Raport setVariableValue(uint nId, double nValue) override
        {
            return mEngine.setVariableValue(nId, nValue);
        }

        Raport setVariableName(uint nId, std::string nName) override
        {
            return mEngine.setVariableName(nId, nName);
        }

        Raport setTemperature(double nTemperature) override
        {
            return mEngine.setTemperature(nTemperature);
        }

        void saveData() override
        {
            mEngine.saveData(mEnvironmentBox->fileName().toStdString(),
                             mEnvironmentBox->numOfPoints());
        }

        void updateWithoutChartBox() override
        {
            unsetSubstrate();
            mEnvironmentBox->update();
            for(auto& variable : mParameterBoxes){
                variable->update();
            }
            for(auto& layer : mLayerBoxes){
                layer->update();
            }
        }

        double transParam(std::string band, std::string param) override
        {
            return mEngine.transParam(band,param);
        }

        void calculateEigens() override
        {
            mEngine.update(mEnvironmentBox->numOfPoints());
            mTransitionsBox->update();
        }

        QtCharts::QAreaSeries* eigenfunction(QString band, double amplitude) override
        {
            QtCharts::QLineSeries* lTop = new QtCharts::QLineSeries{};
            QtCharts::QLineSeries* lGround = new QtCharts::QLineSeries{};

            for(auto& point : mEngine.eigenfunction(band.toStdString(), amplitude)){
                lTop->append(point.mX/10.0, point.mEnergy);
            }

            for(auto& point : mEngine.eigenEnergyLine(band.toStdString())){
                lGround->append(point.mX/10.0, point.mEnergy);
            }

            QtCharts::QAreaSeries *oSeries = new QtCharts::QAreaSeries(lTop, lGround);
            return oSeries;
        }

    private slots:
        void addLayerBox()
        {
            LayerBox* lLayerBox = new LayerBox{*this };
            mLayerBoxes.push_back( lLayerBox );
            mLayout2bb->addWidget( lLayerBox );

            addLayer(lLayerBox->id());
        }

        void addLayerBox(QString formula, QString width, bool substrate)
        {
            LayerBox* lLayerBox = new LayerBox{*this };
            mLayerBoxes.push_back( lLayerBox );
            mLayout2bb->addWidget( lLayerBox );

            addLayer(lLayerBox->id());
            lLayerBox->setParameterts(formula, width, substrate);
        }

        void addParameterBox()
        {
            VariableBox* lParameterBox = new VariableBox{*this };
            mParameterBoxes.push_back( lParameterBox );
            mLayout2ba->addWidget( lParameterBox );

            addVariable(lParameterBox->id());
        }

        void addParameterBox(QString name, QString value)
        {
            VariableBox* lParameterBox = new VariableBox{*this };
            mParameterBoxes.push_back( lParameterBox );
            mLayout2ba->addWidget( lParameterBox );

            addVariable(lParameterBox->id());
            lParameterBox->setParameters(name,value);
        }

        void update()
        {
            unsetSubstrate();
            mEnvironmentBox->update();
            for(auto& variable : mParameterBoxes){
                variable->update();
            }
            for(auto& layer : mLayerBoxes){
                layer->update();
            }

            mChartBox->update();
        }
    };

}

#endif // MUI_MAINLAYOUT

