#ifndef MUI_BASICOBJECT
#define MUI_BASICOBJECT

#include <QString>
#include <QFrame>

#include <QLineEdit>
#include <QVBoxLayout>
#include <QLabel>

#include "mui_imainlayout.h"
#include "common_iconnector.h"

namespace Mui{

    class NamedLineEdit : public QWidget
    {
    private:
        QVBoxLayout* mLayout;
        QLabel* mLabel;
        QLineEdit* mLineEdit;
    public:
        NamedLineEdit(QString nLabel, QString startText)
        {
            mLayout = new QVBoxLayout{ };
            setLayout(mLayout);
            mLabel = new QLabel{startText};
            //mLayout->addWidget(mLabel);
            mLineEdit = new QLineEdit{ };
            mLineEdit->setMaximumHeight(30);
            mLineEdit->setMaximumWidth(200);
            mLineEdit->setPlaceholderText(nLabel);
            mLineEdit->setText(startText);
            mLayout->addWidget(mLineEdit);
        }

        QString text()
        {
            return mLineEdit->text();
        }

        void setText(QString text)
        {
            mLineEdit->setText(text);
        }
    };

    class BasicObject : public QFrame
    {
    protected:
        static uint sLastObjectId;
        uint mId;
        iMainLayout& mMainLayout;
    public:
        BasicObject(iMainLayout& nMainLayout) :
           mId{ 0 }
          , mMainLayout{ nMainLayout }
        {
            mId = sLastObjectId;
            sLastObjectId++;
        }
        virtual ~BasicObject() = default;

        virtual uint id(){ return mId; }
        virtual QString name(){ return ""; }
        virtual void update(){ /*EMPTY*/ }
    };

}


#endif // MUI_BASICOBJECT

