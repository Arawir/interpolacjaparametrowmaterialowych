#ifndef LAYER_CORE
#define LAYER_CORE

#include <sstream>
#include <string>

#include "core_database.h"
#include "core_iengine.h"


namespace Core{

    class Ingredient
    {
    public:
        Database& mDatabase;
        std::vector<std::string> mElement;
        double mContribution;
    public:
        Ingredient(std::vector<std::string> nElement, double nContribution, Database& nDatabase) :
            mDatabase{ nDatabase }
          , mElement{ nElement }
          , mContribution{ nContribution }
        {
        }

        std::string name()
        {
            std::string oName = "";
            for(auto& element : mElement){
                oName += element;
            }
            return oName;
        }

    private:

        void sortElements()
        {
            for(uint j=0; j<mElement.size(); j++){
                uint lMinIndex=1000;
                uint lPosition=0;

                for(uint i=j; i<mElement.size(); i++){
                    uint lIndex = 8*mDatabase.element(mElement[i])->mGroup + mDatabase.element(mElement[i])->mPeriod;
                    if(lIndex < lMinIndex){ lPosition = i; lMinIndex = lIndex; }
                }
                std::swap(mElement[j], mElement[lPosition]);
            }
        }
    };


    class Layer
    {
    public:
        double mWidth = 0.0;
        std::string mLastFormula;
        bool mIsSubstrate = false;
    private:
        iEngine& mEngine;
        Database& mDatabase;
        std::vector<Ingredient> mIngredients;
        std::vector<Ingredient> mTwoElementsIngredients;
        std::vector<Ingredient> mThreeElementsIngredients;
        uint mId;

    public:
        Layer(iEngine& nEngine, Database& nDatabase, uint nId) :
            mEngine{ nEngine }
          , mDatabase{ nDatabase }
          , mId{ nId }
        { }
        ~Layer() = default;

        uint id(){ return mId; }


        double calculateEg()
        {
            return calculateEnergyGap(); //UGLY
        }

        double gridConstant()
        {
            return calculateParameter("gridConstant");
        }

        double ePependicular()
        {
            return -2.0*calculateParameter("c12")/calculateParameter("c11")*eParallel();
        }

        double eParallel()
        {
            if(mDatabase.useSubstrate){
                return (mDatabase.mSubstrateGridConstant-gridConstant())/gridConstant();
            }
            return 0.0;
        }

        double dEch()
        {
            return calculateParameter("ac")*(ePependicular()+2.0*eParallel());
        }

        double dEvh()
        {
            return calculateParameter("av")*(ePependicular()+2.0*eParallel());
        }

        double dEvb()
        {
            return calculateParameter("b")*(ePependicular() - eParallel());
        }

        double dEvbPlus()
        {
            double DELTAso = calculateParameter("DELTAso");
            return (dEvb()-DELTAso +  sqrt(9.0*dEvb()*dEvb()+2.0*dEvb()*DELTAso+DELTAso*DELTAso ) )/2.0;
        }

        double dEvbMinus()
        {
            double DELTAso = calculateParameter("DELTAso");
            return (dEvb()-DELTAso - sqrt(9.0*dEvb()*dEvb()+2.0*dEvb()*DELTAso+DELTAso*DELTAso ) )/2.0;
        }

        double calculateEc()
        {
            //na razie tylko gamma
            double lVBO = calculateParameter("VBO");
            double lEg = calculateParameter("energyGapInGammaPoint");
            return lVBO + lEg + dEch();
        }

        double calculateEhh()
        {
            //na razie tylko gamma
            double lVBO = calculateParameter("VBO");
            return lVBO + dEvh() - dEvb();
        }

        double calculateElh()
        {
            //na razie tylko gamma
            double lVBO = calculateParameter("VBO");
            return lVBO + dEvh() + dEvbPlus();
        }

        double calculateEsh()
        {
            //na razie tylko gamma
            double lVBO = calculateParameter("VBO");
            return lVBO + dEvh() + dEvbMinus();
        }

        double calculateMe()
        {
            //na razie tylko gamma
            return calculateParameter("meInGammaPoint");
            //return 0.0;
        }

        double calculateMlh()
        {
            //na razie tylko gamma
            return calculateParameter("mlhInGammaPoint");
        }

        double calculateMhh()
        {
            //na razie tylko gamma
            return calculateParameter("mhhInGammaPoint");
        }

        Raport setFormula(std::string nFormula)
        {
            std::string lElement;
            std::string lContribution;
            double tContribution;
            bool lNeedToFindContribution=true;

            nFormula = removeSpaces(nFormula);

            mLastFormula = nFormula;

            mIngredients.clear();
            mTwoElementsIngredients.clear();
            mThreeElementsIngredients.clear();

            for(uint i=0; i<nFormula.length(); i++)
            {
                lElement.clear();
                lContribution.clear();

                while(nFormula[i]!='('){
                    if( (nFormula[i] == '-') || (nFormula[i] == '\0')){
                        lNeedToFindContribution = false;
                        tContribution = 1.0;
                        i--;
                        break;
                    }
                    lElement += nFormula[i];
                    i++;
                }
                i++;

                if(lNeedToFindContribution){
                    while(nFormula[i]!=')'){
                        lContribution += nFormula[i];
                        i++;
                    }
                    i++;
                    ////////////////////////////


                    if(lContribution.find_first_of('-') == std::string::npos){
                        if( mEngine.existsParameter(lContribution) ){
                            tContribution = mEngine.variable(lContribution)->mValue;
                        } else {
                            std::istringstream iss(lContribution);
                            iss >> tContribution;
                        }
                    } else if( lContribution.find_first_of('-') == lContribution.find_last_of('-')){
                        if( (lContribution[0]=='1')&&(lContribution[1]=='-') ){
                            lContribution.erase(lContribution.begin(), lContribution.begin()+2);
                            tContribution = 1.0 - mEngine.variable(lContribution)->mValue;
                        } else if( (lContribution[0]=='1')&&(lContribution[1]=='.')&&(lContribution[2]=='0')&&(lContribution[3]=='-') ){
                            lContribution.erase(lContribution.begin(), lContribution.begin()+4);
                            tContribution = 1.0 - mEngine.variable(lContribution)->mValue;
                        } else {
                            assert(false);
                        }
                    } else {
                        //TODO
                        assert(false);
                    }
                }
                lNeedToFindContribution = true;
                mIngredients.push_back( {{lElement}, tContribution, mDatabase} );
            }

            generateElementsCombinations();

            if(!checkIngredientsContributions()){
                assert(false);
                return Raport{ RaportType::Bug };
            }
            return Raport{ RaportType::Ok };
        }

        std::vector<QuantumWellPoint> valenceBandPoints()
        {
            std::vector<QuantumWellPoint> oPoints;
            oPoints.push_back( {mEngine.locationOfBeginningOfLayer(mId), valenceBandEnergy()});
            oPoints.push_back( {mEngine.locationOfEndingOfLayer(mId), valenceBandEnergy()});

            return oPoints;
        }

        std::vector<QuantumWellPoint> conductionBandPoints()
        {
            std::vector<QuantumWellPoint> oPoints;
            oPoints.push_back( {mEngine.locationOfBeginningOfLayer(mId), conductionBandEnergy()});
            oPoints.push_back( {mEngine.locationOfEndingOfLayer(mId), conductionBandEnergy()});

            return oPoints;
        }




    private:
        double calculateParameter(std::string nParameterName)
        {
            if( mDatabase.parameterHasBowing(nParameterName) ){
                return calculateParameterWithBowing(nParameterName);
            }
            return calculateParameterWithoutBowing(nParameterName);
        }

        double calculateParameterWithoutBowing(std::string nParameterName)
        {
            double oParameterValue = 0.0;
            for(auto& ingredient : mTwoElementsIngredients){
                double lIngredientParameter = mDatabase.semiconductor(ingredient.name())->valueOf(nParameterName);

                if (nParameterName == "energyGapInGammaPoint") {
                    double lAlpha = mDatabase.semiconductor(ingredient.name())->valueOf("alphaInGammaPoint");
                    double lBeta = mDatabase.semiconductor(ingredient.name())->valueOf("betaInGammaPoint");
                    double T = mDatabase.mTemperature;
                    lIngredientParameter = lIngredientParameter - lAlpha*T*T/(T+lBeta);
                }

                if (nParameterName == "energyGapInXPoint") {
                    double lAlpha = mDatabase.semiconductor(ingredient.name())->valueOf("alphaInXPoint");
                    double lBeta = mDatabase.semiconductor(ingredient.name())->valueOf("betaInXPoint");
                    double T = mDatabase.mTemperature;
                    lIngredientParameter = lIngredientParameter - lAlpha*T*T/(T+lBeta);
                }

                if (nParameterName == "energyGapInLPoint") {
                    double lAlpha = mDatabase.semiconductor(ingredient.name())->valueOf("alphaInLPoint");
                    double lBeta = mDatabase.semiconductor(ingredient.name())->valueOf("betaInLPoint");
                    double T = mDatabase.mTemperature;
                    lIngredientParameter = lIngredientParameter - lAlpha*T*T/(T+lBeta);
                }

                oParameterValue += ingredient.mContribution * lIngredientParameter;
            }

            return oParameterValue;
        }

        double calculateParameterWithBowing(std::string nParameterName)
        {
            double oParameterValue = calculateParameterWithoutBowing(nParameterName);

            for(auto& ingredient : mThreeElementsIngredients){
                double lIngredientParameterBowing = mDatabase.semiconductor(ingredient.name())->valueOf(nParameterName + "Bowing");
                oParameterValue -= ingredient.mContribution * lIngredientParameterBowing;
            }
            return oParameterValue;
        }

        double calculateEnergyGap()
        {
            double oEnergyGap = calculateParameterWithBowing("energyGapInGammaPoint");
            if( calculateParameterWithBowing("energyGapInXPoint") < oEnergyGap){
                oEnergyGap = calculateParameterWithBowing("energyGapInXPoint");
            }
            if( calculateParameterWithBowing("energyGapInLPoint") < oEnergyGap){
                oEnergyGap = calculateParameterWithBowing("energyGapInLPoint");
            }
            return oEnergyGap;
        }

        double valenceBandEnergy()
        {
            return calculateParameterWithBowing("VBO");
        }

        double conductionBandEnergy()
        {
            return calculateParameterWithBowing("VBO") + calculateEnergyGap();
        }

        void generateElementsCombinations()
        {
            for(uint i=0; i<mIngredients.size(); i++){
                for(uint j=i+1; j<mIngredients.size(); j++){
                    std::string lFirstElement = mIngredients[i].mElement.at(0);
                    std::string lSecondElement = mIngredients[j].mElement[0];
                    double lContribution = mIngredients[i].mContribution*mIngredients[j].mContribution;
                    if( mDatabase.element(lFirstElement)->mGroup != mDatabase.element(lSecondElement)->mGroup){
                        mTwoElementsIngredients.push_back( {{lFirstElement,lSecondElement}, lContribution, mDatabase });
                    }
                }
            }

            for(uint i=0; i<mIngredients.size(); i++){
                for(uint j=i+1; j<mIngredients.size(); j++){
                    for(uint k=j+1; k<mIngredients.size(); k++){
                        std::string lFirstElement = mIngredients[i].mElement[0];
                        std::string lSecondElement = mIngredients[j].mElement[0];
                        std::string lThridElement = mIngredients[k].mElement[0];
                        int lFirstElementGroup = mDatabase.element(lFirstElement)->mGroup;
                        int lSecondElementGroup = mDatabase.element(lSecondElement)->mGroup;
                        int lThridElementGroup = mDatabase.element(lThridElement)->mGroup;
                        double lContribution = mIngredients[i].mContribution*mIngredients[j].mContribution*mIngredients[k].mContribution;
                        if( !((lFirstElementGroup==lSecondElementGroup)&&(lSecondElementGroup==lThridElementGroup)) ){
                            mThreeElementsIngredients.push_back( {{lFirstElement,lSecondElement, lThridElement}, lContribution, mDatabase });
                        }
                    }
                }
            }

        }

        bool checkIngredientsContributions()
        {
            double lContributionSum = 0.0;
            double lContributionVGroup = 0.0;
            double lContributionIIIGroup = 0.0;

            for(auto& ingredient : mIngredients){
                if(ingredient.mContribution < -0.01){ return false; }
                if(mDatabase.element(ingredient.name())->mGroup == 13){
                    lContributionIIIGroup += ingredient.mContribution;
                }
                if(mDatabase.element(ingredient.name())->mGroup == 15){
                    lContributionVGroup += ingredient.mContribution;
                }
            }

            for(auto& ingredient : mTwoElementsIngredients){
                if(ingredient.mContribution < -0.01){ return false; }
                lContributionSum += ingredient.mContribution;
            }


            if( pow((lContributionIIIGroup-1.0),2) > 0.01 ) return false;
            if( pow((lContributionVGroup-1.0),2) > 0.01 ) return false;
            if( pow((lContributionSum-1.0),2) > 0.01 ) return false;
            return true;
        }

        std::string removeSpaces(std::string nText)
        {
            std::string oText = "";

            for(uint i=0; i<nText.size(); i++){
                if(nText[i] != ' '){ oText += nText.at(i); }
            }
            return oText;
        }

    };

}

#endif // LAYER_CORE

