#ifndef MUI_CHART
#define MUI_CHART

#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QAreaSeries>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QComboBox>
#include <QtCharts/QValueAxis>
#include <fstream>

#include "mui_imainlayout.h"

#include "mui_variable.h" //UGLY UGLY UGLY!!!!!
#include "mui_layer.h"

namespace Mui{

    enum class ChartMode
    {
        QuantumWell, SemiconductorEnergies
    };

    class ChartBox : public QFrame
    {
        Q_OBJECT
    private:
        iMainLayout& mMainLayout;
        ChartMode mMode;


        QVBoxLayout* mLayout;
        QHBoxLayout* mLayout2;
        QPushButton* mPlotButton;
        QPushButton* mCalculateEigensButton;

        QPushButton* mSaveButton;
        QLineEdit* mFilePathEdit;

        QtCharts::QChart* mChart;
        QtCharts::QChartView* mChartView;
        QComboBox* mLayerSelector;
        QComboBox* mParameterSelector;



    public:
        ChartBox(iMainLayout& nMainLayout) :
            mMainLayout{ nMainLayout }
          , mMode{ ChartMode::QuantumWell }
        {
            mLayout = new QVBoxLayout{};
            mLayout->setAlignment(Qt::AlignTop);
            setLayout(mLayout);

            mLayout2 = new QHBoxLayout{};
            mLayout->addLayout(mLayout2);


            mPlotButton = new QPushButton{"Plot structure"};
            connect(mPlotButton, SIGNAL(clicked(bool)), this, SLOT(setMode()));
            mLayout2->addWidget(mPlotButton);

            mCalculateEigensButton = new QPushButton{"Calculate"};
            connect(mCalculateEigensButton, SIGNAL(clicked(bool)), this, SLOT(calculateEigens()));
            mLayout2->addWidget(mCalculateEigensButton);

            mLayerSelector = new QComboBox{};
            connect(mLayerSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(plot()));
            mLayout2->addWidget(mLayerSelector);
            mLayerSelector->hide();

            mParameterSelector = new QComboBox{};
            connect(mParameterSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(plot()));
            mLayout2->addWidget(mParameterSelector);
            mParameterSelector->hide();

            mSaveButton = new QPushButton{ "Save data" };
            connect(mSaveButton, SIGNAL(clicked(bool)), this, SLOT(save()));
            mLayout2->addWidget(mSaveButton);

            mFilePathEdit = new QLineEdit{ "./data.txt" };
            mFilePathEdit->setMaximumWidth( mLayout->sizeHint().width()/5);
            //mLayout2->addWidget(mFilePathEdit);

            mChart = new QtCharts::QChart();
            mChart->createDefaultAxes();

            mChartView = new QtCharts::QChartView(mChart);
            mChartView->setRenderHint(QPainter::Antialiasing);
            mLayout->addWidget(mChartView);

            update();

        }

        void update()
        {
            uint tLayerId = mLayerSelector->currentData().toUInt();
            uint tVariableId = mParameterSelector->currentData().toUInt();

            updateParameterList();
            updateLayersList();

            if( mLayerSelector->findData(tLayerId) != -1){
                mLayerSelector->setCurrentIndex( mLayerSelector->findData(tLayerId) );
            }

            if( mParameterSelector->findData(tVariableId) != -1){
                mParameterSelector->setCurrentIndex( mParameterSelector->findData(tVariableId) );
            }

            plot();
        }

    private:
        void updateParameterList()
        {
            mParameterSelector->clear();
            for(auto& parameter : mMainLayout.variables()){
                mParameterSelector->addItem(parameter->name(), parameter->id());
            }
        }

        void updateLayersList()
        {
            mLayerSelector->clear();
            for(auto& layer : mMainLayout.layers()){
                mLayerSelector->addItem(layer->formula(), layer->id());
            }
        }

        void plotStructure()
        {
            mChart->removeAllSeries();
            QtCharts::QLineSeries* lEcSeries = mMainLayout.conQuantumWellPoints();
            QtCharts::QLineSeries* lEhhSeries = mMainLayout.hhQuantumWellPoints();
            QtCharts::QLineSeries* lElhSeries = mMainLayout.lhQuantumWellPoints();
            QtCharts::QLineSeries* lEshSeries = mMainLayout.shQuantumWellPoints();

            mChart->setTitle("Quantum well");
            QFont lFont = mChart->titleFont();
            lFont.setPointSize(14);
            lFont.setBold(true);
            mChart->setTitleFont(lFont);

            lEcSeries->setName("Conduction band");
            lEhhSeries->setName("Heavy holes band");
            lElhSeries->setName("Light holes band");
            lEshSeries->setName("Split-off holes band");

            mChart->addSeries( lEcSeries );
            mChart->addSeries( lEhhSeries );
            mChart->addSeries( lElhSeries );
            mChart->addSeries( lEshSeries );

            bool seriesCbLhAreTheSame = true;
            bool seriesLhHhAreTheSame = true;
            bool seriesHhShAreTheSame = true;
            for(int i=0; i<lEhhSeries->points().size(); i++){
                if(lEcSeries->at(i)!=lElhSeries->at(i)){ seriesCbLhAreTheSame = false; }
                if(lElhSeries->at(i)!=lEhhSeries->at(i)){ seriesLhHhAreTheSame = false; }
                if(lEhhSeries->at(i)!=lEshSeries->at(i)){ seriesHhShAreTheSame = false; }
            }

            if(seriesCbLhAreTheSame){
                QPen pen = lElhSeries->pen();
                pen.setWidth(3*lEcSeries->pen().width());
                lElhSeries->setPen(pen);
            }
            if(seriesLhHhAreTheSame){
                QPen pen = lEhhSeries->pen();
                pen.setWidth(3*lElhSeries->pen().width());
                lEhhSeries->setPen(pen);
            }
            if(seriesHhShAreTheSame){
                QPen pen = lEshSeries->pen();
                pen.setWidth(3*lEhhSeries->pen().width());
                lEshSeries->setPen(pen);
            }

            mChart->createDefaultAxes();

            #pragma GCC diagnostic push
            #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisX() )->setTickCount(11);
            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisX() )->setMinorTickCount(1);
            //mChart->axisX()->setTitleText("Width [\u212B]");
            mChart->axisX()->setTitleText("Width [nm]");

            double lRoundedMaxY = roundedMaxY(lEcSeries,lEhhSeries,lElhSeries,lEshSeries);
            double lRoundedMinY = roundedMinY(lEcSeries,lEhhSeries,lElhSeries,lEshSeries);
            uint lYTicsCount = (lRoundedMaxY-lRoundedMinY)*2+1;

            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->setTickCount(lYTicsCount);
            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->setMinorTickCount(4);
            mChart->axisY()->setRange(lRoundedMinY, lRoundedMaxY);
            mChart->axisY()->setTitleText("Energy [eV]");
            #pragma GCC diagnostic pop

        }

        void plotLayerEnergyStructure()
        {
            uint tLayerId = mLayerSelector->currentData().toUInt();
            uint tVariableId = mParameterSelector->currentData().toUInt();

            if(mMainLayout.layerEnergyStructureIsEneable(tLayerId, tVariableId) ){
                mChart->removeAllSeries();

                QtCharts::QLineSeries* lEcSeries = new QtCharts::QLineSeries{  };
                QtCharts::QLineSeries* lEhhSeries = new QtCharts::QLineSeries{ };
                QtCharts::QLineSeries* lElhSeries = new QtCharts::QLineSeries{ };
                QtCharts::QLineSeries* lEshSeries = new QtCharts::QLineSeries{ };

                mChart->setTitle("Layer energy structure");
                QFont lFont = mChart->titleFont();
                lFont.setPointSize(14);
                lFont.setBold(true);
                mChart->setTitleFont(lFont);


                lEcSeries->setName("Conduction band");
                lEhhSeries->setName("Heavy holes band");
                lElhSeries->setName("Light holes band");
                lEshSeries->setName("Split-off holes band");



                for(auto& data : mMainLayout.layerEnergyStructure(tLayerId, tVariableId) ){
                    lEcSeries->append( {data.mVariable, data.mEc} );
                    lEhhSeries->append( {data.mVariable, data.mEhh} );
                    lElhSeries->append( {data.mVariable, data.mElh} );
                    lEshSeries->append( {data.mVariable, data.mEsh} );
                }

                mChart->addSeries( lEcSeries );
                mChart->addSeries( lEhhSeries );
                mChart->addSeries( lElhSeries );
                mChart->addSeries( lEshSeries );

                QPen pen = lEhhSeries->pen();
                pen.setWidth(6);
                lEhhSeries->setPen(pen);

                mChart->createDefaultAxes();

                #pragma GCC diagnostic push
                #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
                reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisX() )->setTickCount(11);
                reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisX() )->setMinorTickCount(1);
                mChart->axisX()->setRange(0.0, 1.0);
                mChart->axisX()->setTitleText(mParameterSelector->currentText());


                double lRoundedMaxY = roundedMaxY(lEcSeries,lEhhSeries,lElhSeries,lEshSeries);
                double lRoundedMinY = roundedMinY(lEcSeries,lEhhSeries,lElhSeries,lEshSeries);
                uint lYTicsCount = (lRoundedMaxY-lRoundedMinY)*2+1;

                reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->setTickCount(lYTicsCount);
                reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->setMinorTickCount(4);
                mChart->axisY()->setRange(lRoundedMinY, lRoundedMaxY);
                mChart->axisY()->setTitleText("Energy [eV]");
                #pragma GCC diagnostic pop


            } else {
                //TODO
            }
        }

        double roundedMaxY(QtCharts::QLineSeries* nA,
                           QtCharts::QLineSeries* nB,
                           QtCharts::QLineSeries* nC,
                           QtCharts::QLineSeries* nD)
        {
            double oMaxY = nA->at(0).y();

            for(auto& point : nA->points()){ oMaxY = std::max(oMaxY, point.y()); }
            for(auto& point : nB->points()){ oMaxY = std::max(oMaxY, point.y()); }
            for(auto& point : nC->points()){ oMaxY = std::max(oMaxY, point.y()); }
            for(auto& point : nD->points()){ oMaxY = std::max(oMaxY, point.y()); }

            return floor(2.0*oMaxY+1.0)/2.0;
        }

        double roundedMinY(QtCharts::QLineSeries* nA,
                           QtCharts::QLineSeries* nB,
                           QtCharts::QLineSeries* nC,
                           QtCharts::QLineSeries* nD)
        {
            double oMinY = nA->at(0).y();

            for(auto& point : nA->points()){ oMinY = std::min(oMinY, point.y()); }
            for(auto& point : nB->points()){ oMinY = std::min(oMinY, point.y()); }
            for(auto& point : nC->points()){ oMinY = std::min(oMinY, point.y()); }
            for(auto& point : nD->points()){ oMinY = std::min(oMinY, point.y()); }

            return floor(2.0*oMinY)/2.0;
        }



    private slots:
        void setMode()
        {
            if(mMode == ChartMode::QuantumWell){
                if( (mLayerSelector->count()>0)&&(mParameterSelector->count()>0)){
                    mMode = ChartMode::SemiconductorEnergies;
                    mPlotButton->setText("Plot structure energies");
                    mParameterSelector->show();
                    mLayerSelector->show();
                }
            } else{
                mMainLayout.updateWithoutChartBox();
                mMode = ChartMode::QuantumWell;
                mPlotButton->setText("Plot layer energies");
                mParameterSelector->hide();
                mLayerSelector->hide();
            }

            plot();
        }

        void plot()
        {   
            if(mMode == ChartMode::QuantumWell){
                plotStructure();
            } else {
                plotLayerEnergyStructure();
            }
        }

        void save()
        {
            mMainLayout.saveData();
        }

        void calculateEigens()
        {
            #pragma GCC diagnostic push
            #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
            double yMax = reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->max();
            double yMin = reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->min();
            #pragma GCC diagnostic pop

            mMainLayout.calculateEigens();

            QtCharts::QAreaSeries* eigEcSeries = mMainLayout.eigenfunction("cb", (yMax-yMin)/10.0);
            QtCharts::QAreaSeries* eigEhhSeries = mMainLayout.eigenfunction("hh", (yMax-yMin)/10.0);
            QtCharts::QAreaSeries* eigElhSeries = mMainLayout.eigenfunction("lh", (yMax-yMin)/10.0);
            QtCharts::QAreaSeries* eigEshSeries = mMainLayout.eigenfunction("sh", (yMax-yMin)/10.0);

            QColor color = reinterpret_cast<QtCharts::QLineSeries*>( mChart->series().at(0) )->color();
            color.setAlpha(85);
            QPen pen1 = eigEcSeries->pen();
            pen1.setColor(color);
            eigEcSeries->setPen(pen1);
            eigEcSeries->setColor(color);

            color = reinterpret_cast<QtCharts::QLineSeries*>( mChart->series().at(1) )->color();
            color.setAlpha(85);
            QPen pen2 = eigEhhSeries->pen();
            pen2.setColor(color);
            eigEhhSeries->setPen(pen2);
            eigEhhSeries->setColor(color);

            color = reinterpret_cast<QtCharts::QLineSeries*>( mChart->series().at(2) )->color();
            color.setAlpha(85);
            QPen pen3 = eigElhSeries->pen();
            pen3.setColor(color);
            eigElhSeries->setPen(pen3);
            eigElhSeries->setColor(color);

            color = reinterpret_cast<QtCharts::QLineSeries*>( mChart->series().at(3) )->color();
            color.setAlpha(85);
            QPen pen4 = eigEshSeries->pen();
            pen4.setColor(color);
            eigEshSeries->setPen(pen4);
            eigEshSeries->setColor(color);



            mChart->addSeries( eigEcSeries );
            mChart->addSeries( eigEhhSeries );
            mChart->addSeries( eigElhSeries );
            mChart->addSeries( eigEshSeries );


            mChart->createDefaultAxes();

            #pragma GCC diagnostic push
            #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisX() )->setTickCount(11);
            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisX() )->setMinorTickCount(1);
            mChart->axisX()->setTitleText("Width [\u212B]");

            uint lYTicsCount = (yMax-yMin)*2+1;
            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->setTickCount(lYTicsCount);
            reinterpret_cast<QtCharts::QValueAxis*>( mChart->axisY() )->setMinorTickCount(4);
            mChart->axisY()->setRange(yMin, yMax);
            mChart->axisY()->setTitleText("Energy [eV]");
            #pragma GCC diagnostic pop

        }


    };

}

#endif // MUI_CHART

