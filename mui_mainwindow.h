#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "ui_mainwindow.h"
#include "mui_mainlayout.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(Core::iEngine& nEngine) :
        QMainWindow(0),
        ui(new Ui::MainWindow)
    {
        ui->setupUi(this);
        setWindowTitle ( "Interpolation of semiconductor structure parameters" );
        mMainLayout = new Mui::MainLayout{ nEngine };
        setCentralWidget(mMainLayout);
    }

    ~MainWindow()
    {
        delete ui;
    }

private:
    Ui::MainWindow *ui;
    Mui::MainLayout* mMainLayout;
};

#endif // MAINWINDOW_H
