#ifndef CORE_ENGINE
#define CORE_ENGINE

#include "core_iengine.h"
#include "core_layer.h"
#include "quantumwell.h"

namespace Core{

    class Engine : public iEngine, public QuantumWell
    {
        Database& mDatabase;
        std::vector<Layer*> mLayers;
        std::vector<Variable*> mParameters;

    public:
        Engine(Database& nDatabase) :
            iEngine{ }
          , QuantumWell{ }
          , mDatabase{ nDatabase }
        {

        }
        ~Engine()
        {
            for(auto& parameter : mParameters){
                delete parameter;
            }
        }

        void update(uint numOfPoionts) override
        {
            QuantumWell::dz = lengthX()*1.0e-10 / (double)(numOfPoionts-1);
            QuantumWell::setGridPoints(numOfPoionts);

            double x = locationOfBeginningOfLayer(mLayers.front()->id())*1.0e-10;
            for(uint i=0; i<numOfPoionts; i++){
                Layer* layer = layerAtX(x/1.0e-10);
                QuantumWell::z[i] = x;
                QuantumWell::vCb[i] = layer->calculateEc()*q;
                QuantumWell::vLh[i] = layer->calculateElh()*q;
                QuantumWell::vHh[i] = layer->calculateEhh()*q;
                QuantumWell::vSh[i] = layer->calculateEsh()*q;

                QuantumWell::mCb[i] = mLayers[0]->calculateMe();
                QuantumWell::mLh[i] = mLayers[0]->calculateMlh();
                QuantumWell::mHh[i] = mLayers[0]->calculateMhh();
                QuantumWell::mSh[i] = mLayers[0]->calculateMhh(); ////UWAGA!!!!!!!!!!!!!!!!!!!
                x+= dz;
            }

            QuantumWell::recalculate();
        }

        std::vector<QuantumWellPoint> eigenfunction(std::string band, double amplitude) override
        {
            std::vector<QuantumWellPoint> oVector;

            if(band=="cb"){
                double max = *max_element(QuantumWell::eigCb.mVector.begin(), QuantumWell::eigCb.mVector.end());
                double min = *min_element(QuantumWell::eigCb.mVector.begin(), QuantumWell::eigCb.mVector.end());
                double Tmp = std::max( sqrt(max*max), sqrt(min*min));

                for(uint i=0; i<QuantumWell::eigCb.mVector.size(); i++){
                    oVector.push_back({QuantumWell::z[i]/1.0E-10, QuantumWell::eigCb.mVector[i]*amplitude/Tmp + QuantumWell::eigCb.mValue/q});
                }
            }
            if(band=="lh"){
                double max = *max_element(QuantumWell::eigLh.mVector.begin(), QuantumWell::eigLh.mVector.end());
                double min = *min_element(QuantumWell::eigLh.mVector.begin(), QuantumWell::eigLh.mVector.end());
                double Tmp = std::max( sqrt(max*max), sqrt(min*min));

                for(uint i=0; i<QuantumWell::eigLh.mVector.size(); i++){
                    oVector.push_back({QuantumWell::z[i]/1.0E-10, QuantumWell::eigLh.mVector[i]*amplitude/Tmp + QuantumWell::eigLh.mValue/q});
                }
            }
            if(band=="hh"){
                double max = *max_element(QuantumWell::eigHh.mVector.begin(), QuantumWell::eigHh.mVector.end());
                double min = *min_element(QuantumWell::eigHh.mVector.begin(), QuantumWell::eigHh.mVector.end());
                double Tmp = std::max( sqrt(max*max), sqrt(min*min));

                for(uint i=0; i<QuantumWell::eigHh.mVector.size(); i++){
                    oVector.push_back({QuantumWell::z[i]/1.0E-10, QuantumWell::eigHh.mVector[i]*amplitude/Tmp + QuantumWell::eigHh.mValue/q});
                }
            }
            if(band=="sh"){
                double max = *max_element(QuantumWell::eigSh.mVector.begin(), QuantumWell::eigSh.mVector.end());
                double min = *min_element(QuantumWell::eigSh.mVector.begin(), QuantumWell::eigSh.mVector.end());
                double Tmp = std::max( sqrt(max*max), sqrt(min*min));

                for(uint i=0; i<QuantumWell::eigSh.mVector.size(); i++){
                    oVector.push_back({QuantumWell::z[i]/1.0E-10, QuantumWell::eigSh.mVector[i]*amplitude/Tmp + QuantumWell::eigSh.mValue/q});
                }
            }
            return oVector;
        }


        std::vector<QuantumWellPoint> eigenEnergyLine(std::string band) override
        {
            std::vector<QuantumWellPoint> oVector;

            if(band=="cb"){
                oVector.push_back({QuantumWell::z.front()/1.0E-10, QuantumWell::eigCb.mValue/q});
                oVector.push_back({QuantumWell::z.back()/1.0E-10, QuantumWell::eigCb.mValue/q});
            }
            if(band=="lh"){
                oVector.push_back({QuantumWell::z.front()/1.0E-10, QuantumWell::eigLh.mValue/q});
                oVector.push_back({QuantumWell::z.back()/1.0E-10, QuantumWell::eigLh.mValue/q});
            }
            if(band=="hh"){
                oVector.push_back({QuantumWell::z.front()/1.0E-10, QuantumWell::eigHh.mValue/q});
                oVector.push_back({QuantumWell::z.back()/1.0E-10, QuantumWell::eigHh.mValue/q});
            }
            if(band=="sh"){
                oVector.push_back({QuantumWell::z.front()/1.0E-10, QuantumWell::eigSh.mValue/q});
                oVector.push_back({QuantumWell::z.back()/1.0E-10, QuantumWell::eigSh.mValue/q});
            }
            return oVector;
        }

        double transParam(std::string band, std::string param) override
        {
            if(param=="energy"){ return QuantumWell::transEnergyEv(band); }
            if(param=="overlap"){ return QuantumWell::transOverlap(band); }
            if(param=="wavelenght"){ return QuantumWell::transWavelengthUm(band); }

            assert(false); return 0;
        }

        void saveData(std::string filename, uint numberOfPoints) override
        {
            std::fstream file(filename.c_str(), std::ios::out);
            double dX = lengthX() / (double)(numberOfPoints-1);
            double x = locationOfBeginningOfLayer(mLayers.front()->id());

            file << "X[A]  Ec[eV]   Ehh[eV]   Elh[eV]   Esh[eV]" << std::endl;
            for(uint i=0; i<numberOfPoints; i++){
                Layer* layer = layerAtX(x);
                file << x << " "
                     << layer->calculateEc() << " "
                     << layer->calculateEhh() << " "
                     << layer->calculateElh() << " "
                     << layer->calculateEsh() << " "
                     << std::endl;
                x+= dX;
            }

            file.close();
        }

        Raport addLayer(uint nLayerId) override
        {
            Layer* lLayer = new Layer{*this, mDatabase, nLayerId};
            mLayers.push_back( lLayer );
            return Raport{ RaportType::Ok };
        }

        virtual void removeLayer(uint nId)
        {
            Layer* tLayer;

            for(uint i=0; i<mLayers.size(); i++){
                if(mLayers.at(i)->id() == nId){
                    tLayer = mLayers.at(i);
                    mLayers.erase(mLayers.begin()+i);
                }
            }

            delete tLayer;
        }

        Raport setLayerFormula(uint nLayerId, std::string nFormula) override
        {
            return layer(nLayerId)->setFormula(nFormula);
        }

        Raport setLayerWidth(uint nLayerId, double nWidth) override
        {
            if(nWidth>=0.0){
                layer(nLayerId)->mWidth = nWidth;
                return Raport{ RaportType::Ok };
            }
            return Raport{ RaportType::Bug, "Layer width < 0!" };
        }

        Raport setLayerAsSubstrate(uint nLayerId) override
        {
            if(layerExists(nLayerId)){
                unsetSubstrate();
                layer(nLayerId)->mIsSubstrate=true;
                mDatabase.mSubstrateGridConstant = layer(nLayerId)->gridConstant();
                mDatabase.useSubstrate = true;
                return Raport{ RaportType::Ok };
            }
            return Raport{ RaportType::Bug, "Layer with id "+std::to_string(nLayerId)+" does not exist!" };
        }

        void unsetSubstrate() override
        {
            for(auto& tLayer : mLayers){
                tLayer->mIsSubstrate = false;
            }
            mDatabase.useSubstrate = false;
        }

        double layerEc(uint nLayerId) override
        {
            return layer(nLayerId)->calculateEc();
        }

        double layerEhh(uint nLayerId) override
        {
            return layer(nLayerId)->calculateEhh();
        }

        double layerElh(uint nLayerId) override
        {
            return layer(nLayerId)->calculateElh();
        }

        double layerEsh(uint nLayerId) override
        {
            return layer(nLayerId)->calculateEsh();
        }

        double layerEg(uint nLayerId) override
        {
            return layer(nLayerId)->calculateEg();
        }

        double layerMe(uint nLayerId) override
        {
            return layer(nLayerId)->calculateMe();
        }
        double layerMlh(uint nLayerId) override
        {
            return layer(nLayerId)->calculateMlh();
        }
        double layerMhh(uint nLayerId) override
        {
            return layer(nLayerId)->calculateMhh();
        }

        std::vector<EnergyStructurePoint> layerEnergyStructure(uint nLayerId, uint nVariableId) override
        {
            std::vector<EnergyStructurePoint> oData;
            double lVariableAtStart = variable(nVariableId)->mValue;

            bool lSubstrateIsUsed = mDatabase.useSubstrate;
            if(mDatabase.useSubstrate){
                if(substrateLayer()->id() == nLayerId){
                    mDatabase.useSubstrate = false;
                }

            }

            variable(nVariableId)->mValue = 0.0;
            while(variable(nVariableId)->mValue < 1.01){
                Layer* tLayer = layer(nLayerId);
                tLayer->setFormula( tLayer->mLastFormula );
                oData.push_back( {variable(nVariableId)->mValue,
                                  tLayer->calculateEc(),
                                  tLayer->calculateEhh(),
                                  tLayer->calculateElh(),
                                  tLayer->calculateEsh()
                                 } );
                variable(nVariableId)->mValue += 0.01;
            }

            variable(nVariableId)->mValue = lVariableAtStart;
            mDatabase.useSubstrate = lSubstrateIsUsed;
            return oData;
        }

        bool layerEnergyStructureIsEneable(uint nLayerId, uint nVariableId) override
        {
            if(layerExists(nLayerId) && (variableExists(nVariableId)) ){ return true; }
            return false;
        }


        Raport addVariable(uint nId) override
        {
            Variable* lParameter = new Variable{};
            lParameter->mId = nId;
            lParameter->mName = "";
            lParameter->mValue = 0.0;
            mParameters.push_back(lParameter);
            return Raport{ RaportType::Ok };
        }

        Raport setVariableValue(uint nId, double nValue) override
        {
            for(auto& parameter : mParameters){
                if(parameter->mId == nId){
                    parameter->mValue = nValue;
                }
            }
            return Raport{ RaportType::Ok };
        }

        Raport setVariableName(uint nId, std::string nName) override
        {
            if( !variableExists(nId, nName) ){
                variable(nId)->mName = nName;
                return Raport{ RaportType::Ok };
            }

            return Raport{ RaportType::Bug, "Name of vaeiable is in use!" };
        }

        Raport setTemperature(double nTemperature) override
        {
            if(nTemperature < 0.0){
                assert(false);
                return Raport{ RaportType::Bug };
            }
            mDatabase.mTemperature = nTemperature;
            return Raport{ RaportType::Ok };
        }
        //////////////////////////



        std::vector<QuantumWellPoint> hhBandPoints()
        {
            std::vector<QuantumWellPoint> oPoints;

            for(auto& layer : mLayers){
                oPoints.push_back( {locationOfBeginningOfLayer(layer->id()), layer->calculateEhh()} );
                oPoints.push_back( {locationOfEndingOfLayer(layer->id()), layer->calculateEhh()} );
            }
            return oPoints;
        }

        std::vector<QuantumWellPoint> lhBandPoints()
        {
            std::vector<QuantumWellPoint> oPoints;

            for(auto& layer : mLayers){
                oPoints.push_back( {locationOfBeginningOfLayer(layer->id()), layer->calculateElh()} );
                oPoints.push_back( {locationOfEndingOfLayer(layer->id()), layer->calculateElh()} );
            }
            return oPoints;
        }

        std::vector<QuantumWellPoint> shBandPoints()
        {
            std::vector<QuantumWellPoint> oPoints;

            for(auto& layer : mLayers){
                oPoints.push_back( {locationOfBeginningOfLayer(layer->id()), layer->calculateEsh()} );
                oPoints.push_back( {locationOfEndingOfLayer(layer->id()), layer->calculateEsh()} );
            }
            return oPoints;
        }

        std::vector<QuantumWellPoint> conBandPoints() override
        {
            std::vector<QuantumWellPoint> oPoints;

            for(auto& layer : mLayers){
                oPoints.push_back( {locationOfBeginningOfLayer(layer->id()), layer->calculateEc()} );
                oPoints.push_back( {locationOfEndingOfLayer(layer->id()), layer->calculateEc()} );
            }
            return oPoints;
        }

        double locationOfBeginningOfLayer(uint nLayerId) override
        {
            double oDistance=0.0;

            for(auto& tLayer : mLayers){
                if(tLayer->id() == nLayerId){
                    break;
                }
                oDistance += tLayer->mWidth;
            }
            return oDistance;
        }

        double locationOfEndingOfLayer(uint nLayerId) override
        {
            return locationOfBeginningOfLayer(nLayerId) + layer(nLayerId)->mWidth;
        }


        Variable* variable(uint nId)
        {
            for(auto& parameter : mParameters){
                if(parameter->mId == nId){
                    return parameter;
                }
            }
            assert(false);
            return nullptr;
        }

        Variable* variable(std::string nName) override
        {
            for(auto& parameter : mParameters){
                if(parameter->mName == nName){
                    return parameter;
                }
            }
            assert(false);
            return nullptr;
        }

        bool existsParameter(std::string nName) override
        {
            for(auto& parameter : mParameters){
                if(parameter->mName == nName){
                    return true;
                }
            }
            return false;
        }


    private:
        double lengthX()
        {
            return locationOfEndingOfLayer(mLayers.back()->id()) - locationOfBeginningOfLayer(mLayers.front()->id());
        }

        Layer* layerAtX(double x)
        {
            for(auto& layer : mLayers){
                if(locationOfBeginningOfLayer(layer->id()) <= x){
                    if(locationOfEndingOfLayer(layer->id()) >= x){
                        return layer;
                    }
                }
            }

            for(auto& layer : mLayers){
                if(locationOfBeginningOfLayer(layer->id()) <= x-0.0001){
                    if(locationOfEndingOfLayer(layer->id()) >= x-0.0001){
                        return layer;
                    }
                }
            }
            for(auto& layer : mLayers){
                if(locationOfBeginningOfLayer(layer->id()) <= x+0.0001){
                    if(locationOfEndingOfLayer(layer->id()) >= x+0.0001){
                        return layer;
                    }
                }
            }
            assert(false);
            return nullptr;
        }

        Layer* layer(uint nId)
        {
            for(auto& layer : mLayers){
                if(layer->id() == nId){ return layer; }
            }
            assert(!"Cannot find layer with id!");
            return nullptr;
        }

        Layer* substrateLayer()
        {
            for(auto& layer : mLayers){
                if(layer->mIsSubstrate){ return layer; }
            }
            return nullptr;
        }

        bool layerExists(uint nId)
        {
            for(auto& layer : mLayers){
                if(layer->id() == nId){ return true; }
            }
            return false;
        }

        bool variableExists(uint nVariavleId)
        {
            for(auto& parameter : mParameters){
                if( parameter->mId==nVariavleId ){
                    return true;
                }
            }
            return false;
        }

        bool variableExists(uint nVariavleId, std::string nName)
        {
            for(auto& parameter : mParameters){
                if( (parameter->mId!=nVariavleId)&&(parameter->mName == nName) ){
                    return true;
                }
            }
            return false;
        }

    };

}

#endif // CORE_ENGINE

