#ifndef MUI_TRANSITIONS
#define MUI_TRANSITIONS

#include <QHBoxLayout>
#include <QPushButton>
#include <QFrame>
#include <QLineEdit>
#include <QTableWidget>

#include "mui_basicobject.h"

namespace Mui{

    class TransitionsBox : public BasicObject
    {
        Q_OBJECT
    private:
        QHBoxLayout* mLayout;
        QTableWidget *mTable;

    public:
        TransitionsBox(iMainLayout& nMainLayout) :
            BasicObject{ nMainLayout }
        {
            setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

            mLayout = new QHBoxLayout{};
            mLayout->setAlignment(Qt::AlignTop);
            setLayout(mLayout);

            mTable = new QTableWidget{3, 3};
            mTable->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
            mTable->setMinimumWidth(350);
            mTable->setVerticalHeaderLabels({"Cb->Lh","Cb->Hh","Cb->Sh"});
            mTable->setHorizontalHeaderLabels({"Energy [eV]", "Wavelength [um]", "Overlap"});
            mLayout->addWidget(mTable);

            mTable->setItem(0, 0, new QTableWidgetItem("-"));
            mTable->setItem(0, 1, new QTableWidgetItem("-"));
            mTable->setItem(0, 2, new QTableWidgetItem("-"));
            mTable->setItem(1, 0, new QTableWidgetItem("-"));
            mTable->setItem(1, 1, new QTableWidgetItem("-"));
            mTable->setItem(1, 2, new QTableWidgetItem("-"));
            mTable->setItem(2, 0, new QTableWidgetItem("-"));
            mTable->setItem(2, 1, new QTableWidgetItem("-"));
            mTable->setItem(2, 2, new QTableWidgetItem("-"));

        }
        ~TransitionsBox() = default;

        void update()
        {
            mTable->item(0,0)->setText(QString::number(mMainLayout.transParam("lh","energy")));
            mTable->item(1,0)->setText(QString::number(mMainLayout.transParam("hh","energy")));
            mTable->item(2,0)->setText(QString::number(mMainLayout.transParam("sh","energy")));

            mTable->item(0,1)->setText(QString::number(mMainLayout.transParam("lh","wavelenght")));
            mTable->item(1,1)->setText(QString::number(mMainLayout.transParam("hh","wavelenght")));
            mTable->item(2,1)->setText(QString::number(mMainLayout.transParam("sh","wavelenght")));

            mTable->item(0,2)->setText(QString::number(mMainLayout.transParam("lh","overlap")));
            mTable->item(1,2)->setText(QString::number(mMainLayout.transParam("hh","overlap")));
            mTable->item(2,2)->setText(QString::number(mMainLayout.transParam("sh","overlap")));
        }

    };

}

#endif // MUI_TRANSITIONS

